![Just like Mom's](albondigas.jpeg)
# Albondigas

Albondigas, Sonoran Home style



Ingredients 
--------------------------
- 1/2 tsp Oregano
- 1/2 tsp Coriander seed (crushed)
- 1/4 cup Cilantro chopped
- 1 lb Potatoes
- 2 cups Carrots cubed
- 1 lb Ground turkey
- 1/2 Yellow onion
- 2 Zucchini
- 1 Egg
- 4 cups Chicken broth
- 1 cup White rice
- 1/2 tsp salt
- 1/2 tsp pepper

Directions
-------------------------
Combine turkey, egg, coriander seed, oregano, 
1/4 of the rice in a bowl. Mix well then form into
balls about the size of a golf ball.

Combine the meatballs and everything else except
zucchini and cilantro in a pot. 
Bring to a boil, then simmer for 15-20 min.

Once potatoes and carrots are tender add zucchini 
continue simmering for another 5-8 min.

Salt and pepper to taste, add remaining cilantro.
